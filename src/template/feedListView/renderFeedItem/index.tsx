import {View, Text} from 'react-native';
import React from 'react';
import {Avatar} from '../../../components';
import {Like1, Message, More, Repeat, Send2} from 'iconsax-react-native';
import styles from './styles';

const RenderFeedItem = () => {
  return (
    <View style={{marginTop: 12}}>
      <View style={{paddingHorizontal: 16}}>
        <View style={styles.rowCenter}>
          <Avatar
            width={42}
            height={42}
            borderRadius={60}
            imageSource={require('../../../assets/images/t2.png')}
          />
          <View style={styles.nameContainer}>
            <Text style={styles.bold}>Jaggu</Text>
            <Text style={styles.fontSize10}>10 mints</Text>
          </View>
        </View>
      </View>
      {/* <BottomModal
        onDelete={onPressDelete}
        onPressToggleSave={onPressToggleSave}
        visible={showModal}
        closeModal={closeModal}
        feed={item}
        user={item?.user}
        currentUser={currentUser}
      /> */}
    </View>
  );
};

export default RenderFeedItem;
