import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 3,
    margin: 5,
    borderWidth: 2,
    borderRadius: 5,
    borderColor: 'whitesmoke',
  },
  selected: {
    backgroundColor: 'yellow',
    color: 'black',
    fontSize: 18,
    fontWeight: '300',
  },
});

export default styles;
