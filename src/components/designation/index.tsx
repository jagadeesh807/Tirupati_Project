import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import styles from './styles';

const Designation = ({title, isSelected, onPress}) => {
  return (
    <TouchableOpacity
      style={[styles.container, isSelected && styles.selected]}
      onPress={onPress}>
      <Text>{title}</Text>
    </TouchableOpacity>
  );
};
export default Designation;
