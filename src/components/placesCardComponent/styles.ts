import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerStyle: {
    fontSize: 20,
    fontFamily: 'Rubik',
    fontWeight: '500',
  },
  viewAll: {
    fontSize: 18,
    fontFamily: 'Rubik',
    fontWeight: '400',
    color: 'orange',
  },
  imageContainer: {
    width: 80,
    height: 80,
    marginTop: 15,
    marginRight: 15,
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: 'whitesmoke',
  },
  imageStyle: {
    marginTop: 10,
    width: 40,
    height: 40,
    borderRadius: 6,
    borderColor: '#A6BAD0',
  },
  placeName: {
    padding: 4,
    alignItems: 'center',
    fontSize: 10,
    marginTop: 5,
  },
});

export default styles;
