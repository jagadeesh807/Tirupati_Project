// ServicesCardComponent.js
import React from 'react';
import {View, Image, FlatList, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import {SCREENS} from '../../screens';

const ServicesCardComponent = () => {
  const navigation = useNavigation();

  const data = [
    {
      id: 1,
      name: 'Hospitals',
      source: require('../../assets/images/Hospital.png'),
      screen: 'Hospital',
      navigation: SCREENS.ALL_SERVICES,
    },
    {
      id: 2,
      name: 'Gyms',
      source: require('../../assets/images/gym.png'),
      navigation: SCREENS.ALL_SERVICES,
    },
    {
      id: 3,
      name: 'Education',
      source: require('../../assets/images/educa.png'),
      navigation: SCREENS.ALL_SERVICES,
    },
    {
      id: 4,
      name: 'Travel',
      source: require('../../assets/images/Travel.png'),
      navigation: SCREENS.ALL_SERVICES,
    },
  ];

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.imageContainer}
      onPress={() => {
        if (item.navigation) {
          navigation.navigate(SCREENS.ALL_SERVICES);
        }
      }}>
      <Image
        style={styles.imageStyle}
        source={item.source}
        resizeMode="contain"
      />
      <Text style={styles.placeName}>{item.name}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        horizontal={true}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        scrollEnabled={false}
        numColumns={1}
      />
    </View>
  );
};

export default ServicesCardComponent;
