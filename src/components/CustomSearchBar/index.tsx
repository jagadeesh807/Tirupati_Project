import React, {useRef} from 'react';
import {View} from 'react-native';
import SearchBar from 'react-native-search-bar';

const CustomSearchBar = ({
  placeholder,
  onChangeText,
  onSearchButtonPress,
  onCancelButtonPress,
}) => {
  const searchBarRef = useRef(null);

  const handleSearch = () => {
    searchBarRef.current?.blur();
    if (onSearchButtonPress) {
      onSearchButtonPress();
    }
  };

  const handleCancel = () => {
    if (onCancelButtonPress) {
      onCancelButtonPress();
    }
  };

  return (
    <View>
      <SearchBar
        placeholder={placeholder}
        onChangeText={onChangeText}
        onSearchButtonPress={handleSearch}
        onCancelButtonPress={handleCancel}
        ref={searchBarRef}
        hideBackground={true}
      />
    </View>
  );
};

export default CustomSearchBar;
