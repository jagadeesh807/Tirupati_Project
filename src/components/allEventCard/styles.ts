import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 15,
    height: 180,
  },
  backgroundImage: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
  dateContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: 10,
    right: 8,
  },
  clockImage: {
    height: 20,
    width: 20,
  },
  dateText: {
    color: '#ffffff',
  },
  infoContainer: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    padding: 10,
  },
  text: {
    color: '#fff',
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bookNowContainer: {
    flexDirection: 'row',
  },
  bookNowImage: {
    height: 13,
    width: 15,
    resizeMode: 'center',
    marginTop: 4,
  },
});

export default styles;
