import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

const AllEventCard = ({
  backgroundImage,
  clockImage,
  date,
  artistFirstName,
  artistLastName,
  location,
  bookNowImage,
}) => {
  return (
    <View style={styles.cardContainer}>
      <Image source={backgroundImage} style={styles.backgroundImage} />
      <View style={styles.dateContainer}>
        <Image source={clockImage} style={styles.clockImage} />
        <Text style={styles.dateText}>{date}</Text>
      </View>
      <View style={styles.infoContainer}>
        <Text style={styles.text}>{artistFirstName}</Text>
        <Text style={styles.text}>{artistLastName}</Text>
        <View style={styles.footerContainer}>
          <Text style={styles.text}>{location}</Text>
          <View style={styles.bookNowContainer}>
            <Text style={styles.text}>Book Now</Text>
            <Image source={bookNowImage} style={styles.bookNowImage} />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 15,
    height: 180,
  },
  backgroundImage: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
  dateContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: 10,
    right: 8,
  },
  clockImage: {
    height: 20,
    width: 20,
  },
  dateText: {
    color: '#ffffff',
  },
  infoContainer: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    padding: 10,
  },
  text: {
    color: '#fff',
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bookNowContainer: {
    flexDirection: 'row',
  },
  bookNowImage: {
    height: 13,
    width: 15,
    resizeMode: 'center',
    marginTop: 4,
  },
});

export default AllEventCard;
