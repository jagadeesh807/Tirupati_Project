import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import styles from './styles';
import SeeMore from '../atom/seeMore';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {ArrowDown2, ArrowDown3, ArrowUp2} from 'iconsax-react-native';
import {IJobCardComponent} from '../../types';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '../../screens';

const JobsCard = ({
  id,
  jobTitle,
  jobLocation,
  experience,
  jobDescription,
  jobQualification,
  projectName,
  responsibilities,
  fixedBudget,
  variableBudget,
  manager,
  expireDate,
}: IJobCardComponent) => {
  const [expanded, setExpanded] = useState(false);
  const navigation = useNavigation();
  return (
    <View style={styles.contentContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          top: 10,
        }}>
        <Text style={styles.jobTitle}>{jobTitle}</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate(SCREENS.JOB_DETAILS)}>
          <Image
            source={require('../../assets/images/FrontSide.png')}
            style={{height: 20, width: 20}}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.jobDetailsView}>
        <Text style={styles.jobDetails}>{jobLocation}</Text>
        <Text style={styles.jobDetails}>{experience} Years</Text>
      </View>
      {expanded && (
        <View>
          <View style={styles.description}>
            <SeeMore text={jobDescription} />
          </View>
          <View style={{marginTop: 16}}>
            <Text style={styles.jobTitle}>Qualification Required</Text>
            <Text style={styles.qualificatioStyle}>{jobQualification}</Text>
          </View>
          <View style={{marginTop: 16}}>
            <Text style={styles.jobTitle}>Project</Text>
            <Text style={styles.qualificatioStyle}>{projectName}</Text>
          </View>
          <View style={{marginTop: 16}}>
            <Text style={styles.jobTitle}>Roles & Responsibilities </Text>
            <Text style={styles.qualificatioStyle}>{responsibilities}</Text>
          </View>
          <View style={{marginTop: 16}}>
            <Text style={styles.jobTitle}>Budget</Text>
            <Text style={styles.qualificatioStyle}>Fixed - {fixedBudget}</Text>
            <Text style={styles.qualificatioStyle}>
              Variable - {variableBudget}
            </Text>
          </View>
          <View style={{marginTop: 16}}>
            <Text style={styles.jobTitle}>Hiring Manager</Text>
            <Text style={styles.qualificatioStyle}>{manager}</Text>
            {/* <Text style={styles.qualificatioStyle}>Rama V</Text> */}
          </View>
          <View style={styles.bottomView}>
            <Text style={styles.activeTill}>Active till : {expireDate} </Text>
            <TouchableOpacity>
              <View style={{flexDirection: 'row'}}>
                {/* <Text style={styles.refer}>Refer</Text> */}
                {/* <Image
                  style={styles.referImage}
                  source={require('/Users/dws/Desktop/Perch/perch-app/src/assets/images/send.png')}
                /> */}
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
      <View>
        {/* <TouchableOpacity onPress={handleExpandCollapse}>
          <View style={styles.expandButtonBottom}>
            {expanded ? (
              <ArrowUp2 style={{color: '#B9B9B9'}} />
            ) : (
              <ArrowDown2 style={{color: '#B9B9B9'}} />
            )}
          </View>
        </TouchableOpacity> */}
      </View>
      {/* </ScrollView> */}
    </View>
  );
};

export default JobsCard;
