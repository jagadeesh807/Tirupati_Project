// components/WelcomeScreen.js

import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

const ShowAllEventCards = ({image}) => {
  return (
    <View style={styles.viewContainer}>
      <Image source={image} style={styles.backgroundImageContainer} />
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: 150,
    height: 210,
    marginLeft: 5,
  },
  backgroundImageContainer: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 5,
  },
});

export default ShowAllEventCards;
