import {View, ActivityIndicator} from 'react-native';
import React from 'react';

const MyActivityIndicator: React.FC<{visible: boolean}> = ({visible}) => {
  if (visible) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color="gray" />
      </View>
    );
  }
  return null;
};

export default MyActivityIndicator;
