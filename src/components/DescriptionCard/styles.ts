import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 5,
    marginTop: 10,
    borderColor: 'whitesmoke',
    borderWidth: 1.5,
    borderRadius: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    marginTop: 5,
  },
  title: {
    fontWeight: '400',
    fontSize: 14,
    color: 'black',
  },
  viewAll: {
    color: '#A1A1A1',
  },
  description: {
    marginHorizontal: 10,
    marginBottom: 10,
  },
});

export default styles;
