import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

const DescriptionCard = ({description, Content}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>{description}</Text>
        {/* <Text style={styles.viewAll}>View All</Text> */}
      </View>
      <Text style={styles.description}>{Content}</Text>
    </View>
  );
};

export default DescriptionCard;
