import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const TouristSpotCard = ({imageSource, rating, title, location}) => {
  return (
    <View style={styles.cardContainer}>
      <Image source={imageSource} style={styles.image} resizeMode="cover" />
      <View style={styles.textContainer}>
        <Image
          source={require('../../assets/images/Star.png')}
          style={{height: 20, width: 20}}
        />
        <Text style={styles.rating}>{rating}</Text>
      </View>
      <View style={styles.shadowViewContainer}>
        <Text style={styles.title}>{title}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 5,
          }}>
          <Text style={styles.location}>{location}</Text>
          <View style={styles.bottomviewcontainer}>
            <Text style={{color: 'white'}}>Visit Now</Text>
            <Image
              source={require('../../assets/images/LeftSide.png')}
              style={styles.arroeContiner}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    margin: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#fff',
    elevation: 3, // for Android shadow
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  image: {
    width: '100%',
    height: 200,
  },
  textContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: 10,
    right: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  location: {
    fontSize: 16,
    color: 'white',
  },
  rating: {
    fontSize: 14,
    color: 'white',
    marginTop: 2,
  },
  shadowViewContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 15,
  },
  bottomviewcontainer: {
    flexDirection: 'row',
  },
  arroeContiner: {
    height: 14,
    width: 14,
    resizeMode: 'center',
    marginTop: 5,
    justifyContent: 'space-between',
  },
});

export default TouristSpotCard;
