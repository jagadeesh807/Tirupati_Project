import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  container: {
    height: '50%',
    alignItems: 'center',
    marginTop: 10,
  },
  image: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
  ratingContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: 10,
    right: 10,
  },
  star: {
    height: 20,
    width: 20,
  },
  ratingText: {
    color: 'white',
    marginTop: 2,
  },
  infoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 15,
  },
  title: {
    color: 'white',
  },
  locationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  location: {
    color: 'white',
  },
  visitNowContainer: {
    flexDirection: 'row',
  },
  visitNow: {
    color: 'white',
  },
  arrow: {
    height: 14,
    width: 14,
    resizeMode: 'center',
    marginTop: 5,
    justifyContent: 'space-between',
  },
});

export default styles;
