import {View, Image} from 'react-native';
import React from 'react';

const TPTLogo = () => {
  return (
    <View style={{alignItems: 'center'}}>
      <Image
        source={require('../../assets/images/tptLogo.png')}
        style={{width: 60, height: 50}}
      />
    </View>
  );
};

export default TPTLogo;
