import {View, Text, Image} from 'react-native';
import React from 'react';

import Avatar from '../atom/avatar';
import {IAppHeader} from '../../types';
import styles from './style';

const AppHeader = ({profilePic, name, designation, navigation}: IAppHeader) => {
  return (
    <View style={styles.AppHeader}>
      <View style={styles.AppHeaderStyles}>
        <Avatar
          imageSource={require('../../assets/images/t1.png')}
          width={50}
          height={50}
          borderRadius={50}
        />
        <View style={styles.headerStyles}>
          <Text style={styles.perchTextLogo}>Tirupati</Text>
        </View>
      </View>

      {/* <NotificationBing
      width={24}
      height={24}
      color="#454C52"
      onPress={handleNotificationPress}
    /> */}
    </View>
  );
};

export default AppHeader;
