import {StyleSheet} from 'react-native';
import {APP_COLOR} from '../../types/Colors';

export const styles = StyleSheet.create({
  AppHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  AppHeaderStyles: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  headerStyles: {
    alignItems: 'center',
    marginHorizontal: 10,
    flex: 1,
  },
  name: {
    fontSize: 14,
    fontWeight: '500',
    color: 'black',
  },
  primaryText: {
    fontSize: 14,
    fontWeight: '500',
    color: APP_COLOR.text,
  },

  content: {
    fontSize: 10,
    color: APP_COLOR.text,
    marginTop: 2,
  },
  perchTextLogo: {
    // fontFamily: 'Pacifico-Regular',
    fontWeight: '400',
    fontSize: 24,
    color: 'black',
    textAlign: 'center',
    // marginHorizontal: 20,
    marginRight: 40,
  },
  imageContainer: {
    height: 50,
    width: 60,
  },
});

export default styles;
