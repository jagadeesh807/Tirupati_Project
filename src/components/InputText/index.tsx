import React from 'react';
import {KeyboardTypeOptions, TextInput, View} from 'react-native';
import styles from './style';

export interface InputText {
  label?: string;
  placeholder: string;
  keyboardType?: KeyboardTypeOptions | undefined;
  handleOnchange: (e: string) => void;
  value: string;
}

const InputText = ({placeholder, keyboardType, handleOnchange}: InputText) => {
  return (
    <View style={styles.inputwidth}>
      <TextInput
        placeholder={placeholder}
        placeholderTextColor={'#B3B3B3'}
        keyboardType={keyboardType}
        onChangeText={handleOnchange}
        style={styles.input}
        onChangeText={handleOnchange}
      />
    </View>
  );
};

export default InputText;
