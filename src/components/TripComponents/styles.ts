import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  mainviewContainer: {
    width: '48%',
    marginTop: 10,
    borderWidth: 1.5,
    borderColor: 'whitesmoke',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  imageStyleContainer: {
    width: 40,
    height: 40,
    marginTop: 7,
    resizeMode: 'center',
    borderRadius: 5,
  },
  backArrowStyleContainer: {
    width: 20,
    height: 20,
    marginTop: 17,
    resizeMode: 'center',
  },
});

export default styles;
