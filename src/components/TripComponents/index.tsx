import {View, Text, Image} from 'react-native';
import React from 'react';
import styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '../../screens';

const TripComponent = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.mainviewContainer}>
      <Image
        source={require('../../assets/images/Hospital.png')}
        style={styles.imageStyleContainer}
      />
      <Text style={{textAlign: 'center', marginTop: 17}}>Romantic Trips</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate(SCREENS.PLACE_AROUND_LIST)}>
        <Image
          source={require('../../assets/images/BackIcon.png')}
          style={styles.backArrowStyleContainer}
        />
      </TouchableOpacity>
    </View>
  );
};
export default TripComponent;
