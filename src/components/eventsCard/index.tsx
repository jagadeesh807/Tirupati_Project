// components/WelcomeScreen.js

import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

const EventCards = ({image, title}) => {
  return (
    <View style={styles.viewContainer}>
      <Image source={image} style={styles.backgroundImageContainer} />
      <View style={styles.bottomViewContainer}>
        <Text style={{color: 'white'}}>{title}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewContainer: {
    width: 150,
    height: 150,
    marginLeft: 5,
  },
  backgroundImageContainer: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 5,
  },
  bottomViewContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 10,
  },
});

export default EventCards;
