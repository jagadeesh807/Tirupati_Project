import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  viewContainer: {
    width: '45%',
    marginTop: 10,
    height: '40%',
    marginHorizontal: 5,
  },
  backgroundImageContainer: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 5,
  },
  ratingViewContainer: {
    position: 'absolute',
    right: 8,
    flexDirection: 'row',
    marginTop: 5,
    justifyContent: 'space-between',
  },
  bottomviewContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 10,
  },
});

export default styles;
