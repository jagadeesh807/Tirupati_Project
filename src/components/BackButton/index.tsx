import React from 'react';
import {TouchableOpacity, Image, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const BackButton = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity style={styles.button} onPress={() => navigation.goBack()}>
      <Image
        source={require('../../assets/images/BackArrow.png')}
        style={styles.image}
      />
      <Text style={styles.text}>Back</Text>
    </TouchableOpacity>
  );
};


export default BackButton;
