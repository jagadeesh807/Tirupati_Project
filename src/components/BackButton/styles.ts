import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
  },
  image: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
  },
  text: {
    marginLeft: 5,
  },
});

export default styles;
