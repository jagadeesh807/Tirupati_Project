import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  viewContainer: {
    width: 170,
    marginTop: 5,
    height: 150,
    color: 'green',
    borderWidth: 0.5,
    borderColor: 'red',
  },
  backgroundImageContainer: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 5,
  },
  ratingViewContainer: {
    position: 'absolute',
    right: 8,
    flexDirection: 'row',
    marginTop: 5,
    justifyContent: 'space-between',
  },
  bottomviewContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 20,
    marginHorizontal: 5,
  },
  ratingIcon: {
    width: 20,
    height: 20,
    resizeMode: 'center',
  },
  ratingText: {
    color: 'white',
    marginTop: 1,
  },
  placeNameText: {
    color: 'red',
    position: 'absolute',
    marginTop: 15,
  },
  locationText: {
    color: 'red',
    position: 'absolute',
  },
});

export default styles;
