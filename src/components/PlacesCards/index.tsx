import {View, Text, Image} from 'react-native';
import React from 'react';
import styles from './styles';

const PlacesCard = ({imageSource, rating, placeName, location}) => {
  return (
    <View style={styles.viewContainer}>
      <Image source={imageSource} style={styles.backgroundImageContainer} />
      <View style={styles.ratingViewContainer}>
        <Image
          source={require('../../assets/images/Star.png')}
          style={styles.ratingIcon}
        />
        <Text style={styles.ratingText}>{rating}</Text>
      </View>
      <View style={styles.bottomviewContainer}>
        <Text style={styles.placeNameText}>{placeName}</Text>
        <Text style={styles.locationText}>{location}</Text>
      </View>
    </View>
  );
};
export default PlacesCard;
