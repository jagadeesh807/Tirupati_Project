import axios from 'axios';
import {BaseURL} from '../../utils/apiConstant';

const axiosInstance = axios.create({
  // Configuration
  baseURL: BaseURL,
  timeout: 30000,
  headers: {
    Accept: '*/*',
    'Content-Type': 'application/json',
  },
});

export default axiosInstance;
