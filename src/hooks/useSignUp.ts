import {useState} from 'react';
import { emailRegex, passwordRegex } from '../utils/validations';

const [userName, setUserName] = useState('');
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [userNameError, setUserNameError] = useState('');
const [emailError, setEmailError] = useState('');
const [passwordError, setPasswordError] = useState('');


const useSignUp = () => {
let isValid = true;

    if (userName.trim() === '') {
      setUserNameError('Enter UserName');
      isValid = false;
    } else if (userName.length < 3) {
      setUserNameError('UserName Should be at least 3 characters');
      isValid = false;
    } else {
      setUserNameError('');
    }

    if (email.trim() === '') {
      setEmailError('Enter Email');
      isValid = false;
    } else if (!emailRegex.test(email)) {
      setEmailError('Invalid email format');
      isValid = false;
    } else {
      setEmailError('');
    }

    if (password.trim() === '') {
      setPasswordError('Enter Password');
      isValid = false;
    } else if (!passwordRegex.test(password)) {
      setPasswordError('Invalid Password');
      isValid = false;
    } else {
      setPasswordError('');
    }

    return {
        setUserName,
        setPassword
    }

};



export default useSignUp;