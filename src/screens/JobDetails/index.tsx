import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import {useRoute} from '@react-navigation/native';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import TPTLogo from '../../components/CustomLogo';
import Designation from '../../components/designation';
import DescriptionCard from '../../components/DescriptionCard';
import BackButton from '../../components/BackButton';

const JobDetails = () => {
  const titles = ['Engineering', 'Sales', 'Support System'];
  const route = useRoute();
  const {job} = route.params; // Access passed job data
  const [selectedTitle, setSelectedTitle] = useState(null);

  return (
    <WrapperContainer>
      <View style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center', padding: 10}}>
          <BackButton />
          <View style={{flex: 1, alignItems: 'center'}}>
            <TPTLogo />
          </View>
          <View style={{width: 60}} />
        </View>
        <View style={{paddingTop: 15}}>
          <FlatList
            data={titles}
            renderItem={({item}) => (
              <Designation
                title={item}
                isSelected={item === selectedTitle}
                onPress={() => setSelectedTitle(item)}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            contentContainerStyle={{
              paddingHorizontal: 10,
            }}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <ScrollView
          style={{marginHorizontal: 10, marginTop: 15}}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              backgroundColor: '#D75A64',
              borderRadius: 5,
              padding: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{color: 'white'}}>{job.role}</Text>
              <Text style={{color: 'white'}}>{job.companyName}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{color: 'white'}}>{job.location}</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={require('../../assets/images/Clock.png')}
                  style={{height: 20, width: 20}}
                />
                <Text style={{color: 'white', marginLeft: 5}}>
                  {job.experience} Years
                </Text>
              </View>
            </View>
          </View>
          <DescriptionCard
            description={'Description'}
            Content={job.description}
          />
          <DescriptionCard
            description={'Qualification Required'}
            Content={job.qualification}
          />
          <DescriptionCard
            description={'CompanyName'}
            Content={job.companyName}
          />
          <DescriptionCard
            description={'IndustryType'}
            Content={job.industryType}
          />
          <DescriptionCard
            description={'Roles & Responsibilities'}
            Content={job.responsibilities}
          />
          <DescriptionCard description={'Budget'} Content={job.budget} />
          <DescriptionCard
            description={'HiringManager'}
            Content={job.hiringManager}
          />
          <DescriptionCard description={'Openings'} Content={job.openings} />
          <DescriptionCard
            description={'TotalEmployees'}
            Content={job.totalEmployees}
          />
          <DescriptionCard
            description={'ExpireDate'}
            Content={job.expireDate}
          />
          <DescriptionCard description={'JobType'} Content={job.jobType} />
        </ScrollView>
      </View>
    </WrapperContainer>
  );
};

export default JobDetails;
