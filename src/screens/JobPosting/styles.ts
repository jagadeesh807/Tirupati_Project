import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  viewContainer: {
    marginHorizontal: 21,
    marginTop: 30,
    paddingVertical: 10,
  },
  text: {
    textAlign: 'center',
  },
  contentContainer: {
    margin: 20,
    padding: 10,
    borderWidth: 0.5,
    borderColor: '#A9A9A9',
    borderRadius: 8,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    fontSize: 18,
    color: '#333', // Adjust the color to match your design
  },
  listContainer: {
    paddingHorizontal: 10, // Adjust this value to increase/decrease space
  },
  titleContainer: {
    padding: 10,
    borderRadius: 5,
    marginRight: 10,
  },
  jobDetailsView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  jobTitle: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '500',
    color: '#EBA756',
  },
  jobDetails: {
    // font-family: Rubik;
    fontSize: 14,
    fontWeight: '300',
    color: '#595959',
  },
});

export default styles;
