import {
  View,
  FlatList,
  Alert,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Designation from '../../components/designation';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import styles from './styles';
import JobsCard from '../../components/jobsCard';
import TPTLogo from '../../components/CustomLogo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '..';

const InternalJob = () => {
  const titles = ['Engineering', 'Sales', 'Support System'];
  const [jobs, setJobs] = useState([]);
  const [selectedTitle, setSelectedTitle] = useState(null);
  const navigation = useNavigation();

  const fetchJobs = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (!token) {
        throw new Error('Token not found in AsyncStorage');
      }

      const response = await fetch('http://35.155.73.184/api/jobs', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const data = await response.json();
      console.log('Fetched Data:', data); // Debugging output

      if (response.ok) {
        setJobs(data.response); // Correctly setting jobs state with the data provided
      } else {
        throw new Error('Failed to fetch jobs');
      }
    } catch (error) {
      console.error('Error fetching jobs:', error);
      Alert.alert('Error', 'Failed to fetch jobs');
    }
  };

  useEffect(() => {
    fetchJobs(); // Fetch jobs when component mounts
  }, []);

  const renderItem = ({item}) => (
    <WrapperContainer>
      <View>
        {/* <JobsCard
          id={item._id}
          jobTitle={item.role}
          jobLocation={item.location}
          experience={item.experience}
          jobType={item.jobType}
          jobDescription={item.description}
          jobQualification={item.qualification}
          projectName={item.companyName}
          responsibilities={item.responsibilities}
          fixedBudget={item.budget}
          variableBudget={null}
          manager={item.hiringManager}
          expireDate={item.expireDate}
          item={item}
        /> */}
        <View
          style={{
            borderColor: 'whitesmoke',
            borderWidth: 2,
            padding: 10,
            borderRadius: 8,
          }}>
          <View style={{marginHorizontal: 10}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                top: 5,
              }}>
              <Text style={styles.jobTitle}>{item.role}</Text>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate(SCREENS.JOB_DETAILS, {job: item})
                }>
                <Image
                  source={require('../../assets/images/FrontSide.png')}
                  style={{height: 20, width: 20}}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.jobDetailsView}>
              <Text style={styles.jobDetails}>{item.location}</Text>
              <Text style={styles.jobDetails}>{item.experience} Years</Text>
            </View>
          </View>
        </View>
      </View>
    </WrapperContainer>
  );

  const EmptyScreen = () => (
    <WrapperContainer>
      <View style={styles.emptyContainer}>
        <Text>No jobs available</Text>
      </View>
    </WrapperContainer>
  );

  return (
    <WrapperContainer>
      <View>
        <TPTLogo />
        <View style={{marginTop: 10}}>
          <FlatList
            data={titles}
            renderItem={({item}) => (
              <Designation
                title={item}
                isSelected={item === selectedTitle}
                onPress={() => setSelectedTitle(item)}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            contentContainerStyle={styles.listContainer}
            showsHorizontalScrollIndicator={false}
          />
          {jobs.length > 0 ? (
            <FlatList
              data={jobs}
              renderItem={renderItem}
              keyExtractor={item => item._id.toString()}
            />
          ) : (
            <EmptyScreen />
          )}
        </View>
      </View>
    </WrapperContainer>
  );
};

export default InternalJob;
