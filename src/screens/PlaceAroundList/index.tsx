import React from 'react';
import {View, FlatList} from 'react-native';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import BackButton from '../../components/BackButton';
import TPTLogo from '../../components/CustomLogo';
import TouristSpotCard from '../../components/TouristSpotCard';

const PLACE_DATA = [
  {
    id: '1',
    imageSource: require('../../assets/images/Tirupati1.jpg'),
    rating: 4.5,
    title: 'Beautiful Beach',
    location: 'Location A',
  },
  {
    id: '2',
    imageSource: require('../../assets/images/Tirupati1.jpg'),
    rating: 4.0,
    title: 'Historic Museum',
    location: 'Location B',
  },
  // Add more tourist spots as needed
];

const PlaceAroundList = () => {
  const renderItem = ({item}) => (
    <TouristSpotCard
      imageSource={item.imageSource}
      rating={item.rating}
      title={item.title}
      location={item.location}
    />
  );

  return (
    <WrapperContainer>
      <View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <BackButton />
          <View style={{flex: 1, alignItems: 'center'}}>
            <TPTLogo />
          </View>
          <View style={{width: 20}} />
        </View>
        <FlatList
          data={PLACE_DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </WrapperContainer>
  );
};

export default PlaceAroundList;
