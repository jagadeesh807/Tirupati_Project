import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
  },
  placeholder: {
    width: 40,
  },
  searchBar: {
    marginTop: 20,
  },
  listContent: {
    marginTop: 15,
    paddingBottom: 20, // To provide some bottom padding if necessary
  },
});
export default styles;
