import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  TitleContainer: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
  },
});
export default styles;
