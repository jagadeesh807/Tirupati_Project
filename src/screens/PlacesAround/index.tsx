import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Image,
} from 'react-native';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import PlacesCard from '../../components/PlacesCards';
import CustomSearchBar from '../../components/CustomSearchBar';
import TPTLogo from '../../components/CustomLogo';
import Designation from '../../components/designation';
import styles from './styles';
import TripComponent from '../../components/TripComponents';

const PlacesAround = () => {
  const titles = ['SV Zoo', 'Park', 'Museum', 'Beach', 'Cafe'];
  const [selectedTitle, setSelectedTitle] = useState(null);

  // Sample data for PlacesCard components
  const placesData = [
    {
      id: '1',
      imageSource: require('../../assets/images/t1.png'),
      rating: 4.3,
      placeName: 'Place 1',
      location: 'Location 1',
    },
    {
      id: '2',
      imageSource: require('../../assets/images/t1.png'),
      rating: 4.3,
      placeName: 'Place 2',
      location: 'Location 2',
    },
    {
      id: '3',
      imageSource: require('../../assets/images/t1.png'),
      rating: 4.3,
      placeName: 'Place 3',
      location: 'Location 3',
    },
  ];

  return (
    <WrapperContainer>
      <ScrollView showsVerticalScrollIndicator={false}>
        <TPTLogo />
        <View>
          <CustomSearchBar
            placeholder={'undefined'}
            onChangeText={undefined}
            onSearchButtonPress={undefined}
            onCancelButtonPress={undefined}
          />
          <View style={{marginTop: 10}}>
            <FlatList
              data={titles}
              renderItem={({item}) => (
                <Designation
                  title={item}
                  isSelected={item === selectedTitle}
                  onPress={() => setSelectedTitle(item)}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
              horizontal={true}
              contentContainerStyle={{paddingHorizontal: 10}}
              showsHorizontalScrollIndicator={false}
            />
          </View>
          <View style={styles.TitleContainer}>
            <Text style={{fontSize: 16, fontWeight: '400', color: '#242424'}}>
              Trending Destinations
            </Text>
            <TouchableOpacity>
              <Text style={{fontSize: 16, fontWeight: '500', color: '#EE4266'}}>
                See all
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={placesData}
            renderItem={({item}) => (
              <PlacesCard
                imageSource={item.imageSource}
                rating={item.rating}
                placeName={item.placeName}
                location={item.location}
              />
            )}
            keyExtractor={item => item.id}
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{paddingHorizontal: 5}}
            ItemSeparatorComponent={() => <View style={{width: 10}} />}
          />
          <View style={{marginTop: 30}}>
            <Text
              style={{
                fontWeight: '400',
                fontSize: 18,
                color: '#242424',
                marginHorizontal: 10,
              }}>
              Which trip would you love to go on?
            </Text>
            <Text style={{marginTop: 5, marginHorizontal: 10}}>
              Hurry, explore our range of trips
            </Text>
            <View
              style={{
                flexWrap: 'wrap-reverse',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginTop: 10,
              }}>
              <TripComponent />
              <TripComponent />
              <TripComponent />
              <TripComponent />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 10,
              }}>
              <Text>Explore Tirupathi</Text>
              <Text style={{color: '#EE4266'}}>See all</Text>
            </View>
            <View
              style={{
                flexWrap: 'wrap-reverse',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginTop: 10,
              }}>
              <PlacesCard />
              <PlacesCard />
            </View>
          </View>
        </View>
      </ScrollView>
    </WrapperContainer>
  );
};

export default PlacesAround;
