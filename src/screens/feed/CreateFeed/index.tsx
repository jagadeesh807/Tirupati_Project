import React, {useState} from 'react';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import WrapperContainer from '../../../components/WrapperContainer/WrapperContainer';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Camera, Gallery} from 'iconsax-react-native';
import PostType from '../../../template/createFeed/postType'; // Assuming this component exists
import styles from './styles';
import {SCREENS} from '../..';

const CreateFeed = () => {
  const [selectImage, setSelectImage] = useState();
  const [content, setContent] = useState('');
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const openCamera = () => {
    let options = {
      mediaType: 'photo',
      quality: 0.5,
    };
    launchCamera(options, response => {
      if (response && response.assets && response.assets.length > 0) {
        setSelectImage(response.assets[0].uri);
        console.log(response);
      } else {
        console.log('No image selected or captured');
      }
    });
  };

  const imagePicker = () => {
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, response => {
      if (response && response.assets && response.assets.length > 0) {
        setSelectImage(response.assets[0].uri);
        console.log(response);
      }
    });
  };

  const onPress = async () => {
    try {
      setLoading(true);
      const id = await AsyncStorage.getItem('userId');
      if (!id) {
        throw new Error('Id not found in AsyncStorage');
      }

      if (!selectImage) {
        throw new Error('No image selected');
      }

      const token = await AsyncStorage.getItem('token');
      if (!token) {
        throw new Error('Token not found in AsyncStorage');
      }

      const formData = new FormData();
      formData.append('user_id', id);
      formData.append('content', content);
      formData.append('file', {
        uri: selectImage,
        name: 'file.jpg',
        type: 'image/jpeg',
      });
      const response = await fetch('http://35.155.73.184/api/addpost', {
        method: 'POST',
        body: formData,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const responseData = await response.json();
      if (response.ok) {
        navigation.navigate(SCREENS.FEED), console.log('Post successful');
        console.log(responseData);
      } else {
        throw new Error(`Post failed: ${responseData.message}`);
      }
    } catch (error) {
      console.error('Post request error:', error);
      Alert.alert('Error', error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <WrapperContainer>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.button}>Cancel</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={onPress} disabled={loading}>
          <Text style={[styles.button, loading && {opacity: 0.5}]}>Post</Text>
        </TouchableOpacity>
      </View>

      <PostType selectedType={''} onToggle={() => {}} url={0} />

      <KeyboardAwareScrollView style={{flex: 1}}>
        <TextInput
          placeholder={"What's on your mind?"}
          style={styles.inputText}
          onChangeText={text => setContent(text)}
          multiline={true}
          textAlignVertical="top"
        />
        {selectImage && (
          <Image style={styles.image} source={{uri: selectImage}} />
        )}
      </KeyboardAwareScrollView>

      <View>
        <View style={styles.imageAndGalleryContainer}>
          <TouchableOpacity style={styles.card} onPress={openCamera}>
            <Camera size={20} color="#595959" />
            <Text style={styles.cardText}>Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.card} onPress={imagePicker}>
            <Gallery size={20} color="#595959" />
            <Text style={styles.cardText}>Gallery</Text>
          </TouchableOpacity>
        </View>
      </View>

      {loading && (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color="#242424" />
        </View>
      )}
    </WrapperContainer>
  );
};

export default CreateFeed;
