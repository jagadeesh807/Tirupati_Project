import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  // container: {
  //   // font-family: Rubik;
  //   // position: 'relative',
  //   backgroundColor: 'white',
  //   padding: 16,
  //   // margin: 16,
  //   // paddingVertical: 16,
  //   // paddingHorizontal: 20,
  //   // paddingBottom:40,
  //   flex: 1,
  //   // height: WINDOW_HEIGHT,
  //   // flexDirection: 'column',
  // },
  // headerContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  //   // marginTop: Platform.OS === 'ios' ? 30 : 5,
  // },
  // postTypeContainer: {
  //   marginTop: 16,
  //   flexDirection: 'row',
  // },
  // postSelector: {
  //   borderWidth: 1,
  //   borderColor: 'red',
  //   // width: IMAGE_WIDTH,
  //   opacity: 0.5,
  //   height: 24,
  //   marginHorizontal: 8,
  //   borderRadius: 12,
  //   // marginVertical: 10,
  //   alignItems: 'center',
  //   paddingLeft: 12,
  //   paddingRight: 4,
  //   flexDirection: 'row',
  //   justifyContent: 'space-evenly',
  // },
  // text: {
  //   fontSize: 10,
  //   fontWeight: '400',
  //   color: 'red',
  //   marginRight: 2,
  // },
  // inputText: {
  //   // marginTop: 16,
  //   color: '#454C52',
  //   fontFamily: 'Rubik',
  //   fontSize: 14,
  //   fontWeight: '400',
  //   letterSpacing: 0.5,
  //   flex: 1,
  //   maxHeight: 100,
  // },
  // card: {
  //   borderWidth: 1,
  //   width: 96,
  //   height: 72,
  //   alignItems: 'center',
  //   padding: 22,
  //   borderRadius: 8,
  //   marginRight: 8,
  //   borderColor: '#0074B7',
  // },
  // cardText: {
  //   fontWeight: '400',
  //   fontSize: 10,
  //   color: '#0074B7',
  // },
  // ImageAndGalleryContainer: {
  //   flexDirection: 'row',
  //   position: 'absolute',
  //   bottom: 20,
  //   left: 0,
  // },
  // imageInFeedContainer: {
  //   position: 'relative',
  //   borderRadius: 8,
  //   overflow: 'hidden',
  //   // height: SCREEN_WIDTH,
  //   //it depends on parent container 20 padding each side
  //   // width: SCREEN_WIDTH - 40,
  //   // height: SCREEN_WIDTH - 40,
  //   // height: 805 * (164 / dimensions.height),
  // },
  // imageInFeed: {
  //   // marginTop: 12,
  //   height: '100%',
  //   width: '100%',
  //   borderRadius: 12,
  // },
  // delImgInCreateFeed: {
  //   position: 'absolute',
  //   top: 10,
  //   right: 10,
  //   backgroundColor: '#0008',
  //   borderRadius: 60,
  //   // height: 20,
  //   // width: 20,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  // outlineCircle: {
  //   borderColor: '#fff',
  //   borderRadius: 60,
  //   height: 18,
  //   width: 18,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  // imageAndGalleryContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   // Add any other styles you need for this container
  // },

  button: {
    borderWidth: 0.5,
    borderRadius: 5,
    padding: 8,
    backgroundColor: '#EE8659',
    color: 'white',
  },
  inputText: {
    marginTop: 10,
    borderRadius: 5,
    padding: 8,
    marginBottom: 16,
  },
  loaderContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  image: {
    width: '100%',
    height: 150,
    marginBottom: 16,
  },
  imageAndGalleryContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start', // start horizontally
    marginBottom: 16, // add margin bottom for spacing
  },
  card: {
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 5,
    padding: 8,
    marginRight: 8,
  },
  cardText: {
    marginLeft: 8,
  },
});

export default styles;
