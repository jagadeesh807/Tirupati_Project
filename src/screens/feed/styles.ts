import {Dimensions, StyleSheet} from 'react-native';
import {SCREEN_WIDTH} from '../../constant/helperConstant';
const {width: screenWidth, height: screenHeight} = Dimensions.get('window');

const styles = StyleSheet.create({
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  nameContainer: {
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 12,
  },
  bold: {
    fontWeight: 'bold',
    color: 'black',
  },
  fontSize10: {fontWeight: 'normal', fontSize: 10, color: '#454C52'},
  feedImage: {
    width: '100%',
    height: SCREEN_WIDTH,
    resizeMode: 'stretch',
    marginTop: 12,
    backgroundColor: 'red',
  },
  likeShareContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginTop: 12,
  },
  likeText: {
    marginTop: 8,
    fontSize: 10,
    color: '#454C52',
    marginHorizontal: 16,
  },
  rowCenters: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  nameContainers: {
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 12,
  },
  bolds: {
    fontWeight: 'bold',
    color: 'black',
  },
  feedImages: {
    width: '100%',
    resizeMode: 'stretch',
    marginTop: 3,
  },
  likeShareContainers: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginTop: 10,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    fontSize: 18,
    color: '#333', // Adjust the color to match your design
  },
});

export default styles;
