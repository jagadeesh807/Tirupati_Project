import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {AppHeader, Avatar, FabButton} from '../../components';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import styles from './styles';
import {Like1, Message, Repeat, Send2} from 'iconsax-react-native';
import {SCREENS} from '..';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Feed = () => {
  const navigation = useNavigation();
  const [feeds, setFeeds] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [imageHeights, setImageHeights] = useState({});

  const fetchPosts = useCallback(async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (!token) {
        throw new Error('Token not found in AsyncStorage');
      }

      const response = await fetch('http://35.155.73.184/api/getposts', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const responseData = await response.json();
      if (response.ok) {
        const validFeeds = responseData.response.map((item, index) => ({
          ...item,
          id: item.id ?? `default-id-${index}`,
        }));

        setFeeds(validFeeds.reverse());
        const userIds = validFeeds.map(item => item.user_id);
        await AsyncStorage.setItem('user_ids', JSON.stringify(userIds));
        console.log('userId check', userIds);
      } else {
        throw new Error(responseData.message || 'Failed to fetch posts');
      }
    } catch (error) {
      console.error('Fetch posts error:', error);
      Alert.alert('Error', error.message);
    } finally {
      setRefreshing(false);
    }
  }, []);

  useEffect(() => {
    fetchPosts();
  }, [fetchPosts]);

  const handleRefresh = () => {
    setRefreshing(true);
    fetchPosts();
  };

  const setImageHeight = (id, width, height) => {
    const screenWidth = Dimensions.get('window').width;
    const desiredWidth = screenWidth - 20; // Adjust this value as needed
    const aspectRatio = height / width;
    const desiredHeight = desiredWidth * aspectRatio;

    setImageHeights(prevState => ({
      ...prevState,
      [id]: desiredHeight,
    }));
  };

  const renderItem = ({item}) => {
    if (!item.user_id) {
      return null; // Skip rendering this item if user_id is null
    }

    const handleImageLoad = event => {
      const {width, height} = event.nativeEvent.source;
      setImageHeight(item.id, width, height);
    };

    return (
      <View key={item.id} style={{marginTop: 15}}>
        <View style={styles.rowCenters}>
          <Avatar
            width={42}
            height={42}
            borderRadius={60}
            imageSource={{uri: item.avatarSource}} // Use URI for network image
          />
          <View style={styles.nameContainers}>
            <Text style={styles.bolds}>{item.user_id.userName}</Text>
            <Text style={styles.fontSize10}>{item.user_id.createdAt}</Text>
          </View>
          <Image
            style={{width: 14, height: 18, marginRight: 5}}
            source={require('../../assets/images/Save.png')}
          />
        </View>
        <Text style={{marginHorizontal: 10, marginTop: 15}}>
          {item.content}
        </Text>
        <Image
          source={{uri: item.imageUrl}}
          style={[
            styles.feedImage,
            {height: imageHeights[item.id] || 100}, // Default height if not calculated yet
          ]}
          onLoad={handleImageLoad}
        />
        <View style={styles.likeShareContainers}>
          <TouchableOpacity
            style={{flexDirection: 'column', alignItems: 'center'}}>
            <Like1 color="#454C52" size={20} />
            <Text>Like</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flexDirection: 'column', alignItems: 'center'}}>
            <Message color="#454C52" size={20} />
            <Text>Comment</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flexDirection: 'column', alignItems: 'center'}}>
            <Repeat color="#454C52" size={20} />
            <Text>Repost</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flexDirection: 'column', alignItems: 'center'}}>
            <Send2 color="#454C52" size={20} />
            <Text>Share</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{backgroundColor: 'gray', height: 0.5, marginTop: 10}}></View>
      </View>
    );
  };

  return (
    <WrapperContainer containerStyle={{flex: 1, padding: 0}}>
      <AppHeader profilePic={''} />
      {feeds.length > 0 ? (
        <FlatList
          data={feeds}
          renderItem={renderItem}
          keyExtractor={item => item.id?.toString() || item.defaultId}
          contentContainerStyle={{paddingBottom: 100}}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
          }
        />
      ) : (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No feeds available</Text>
        </View>
      )}
      <FabButton onPress={() => navigation.navigate(SCREENS.CREATE_FEED)} />
    </WrapperContainer>
  );
};

export default Feed;
