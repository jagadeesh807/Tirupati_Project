import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    flex: 1,
  },
  logoStyle: {
    alignSelf: 'center',
  },
  forgotPasswordText: {
    fontFamily: 'Rubik',
    fontSize: 18,
    fontWeight: '600',
    paddingHorizontal: 22,
    paddingTop: 2,
    color: '#000000',
  },
  loginText: {
    fontFamily: 'Rubik',
    fontSize: 18,
    fontWeight: '600',
    paddingTop: 20,
    color: '#151615',
    paddingHorizontal: 16,
  },
  resetPassowrd: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '600',
    paddingTop: 15,
    color: '#989898',
    paddingHorizontal: 20,
  },
  emailText: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '500',
    paddingHorizontal: 20,
    padding: 8,
    color: '#595959',
  },
  errorStyle: {
    color: 'red',
    marginHorizontal: 20,
  },
  buttonStyle: {
    backgroundColor: '#EE4266',
    // height: 50,
  },
});

export default styles;