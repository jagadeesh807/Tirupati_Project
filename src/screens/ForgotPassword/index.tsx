import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import styles from './styles';
import ButtonLoader from '../../components/ButtonLoader';
import InputText from '../../components/InputText';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '..';
import {emailRegex} from '../../utils/validations';
import axiosInstance from '../../data/service/api';
import {API_URL} from '../../utils/apiConstant';
import MyActivityIndicator from '../../components/ActivityIndicator';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ForgotPassword = () => {
  // const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // Fetch token from AsyncStorage when component mounts
    fetchToken();
  }, []);

  const fetchToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      // Set token as default authorization header for Axios instance
      axiosInstance.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${token}`;
    } catch (error) {
      console.error('Error retrieving token:', error);
    }
  };

  const handleSignIn = async () => {
    const isEmailValid = validateEmail();

    if (!isEmailValid) {
      return;
    }

    // Show loading indicator
    setLoading(true);

    try {
      const response = await axiosInstance.post(API_URL.EMAIL_VERIFICATION, {
        email: email,
      });

      if (response.status === 200) {
        Alert.alert('Verification link', response.data.toString());
      } else {
        Alert.alert('Error', response.data.toString());
      }
    } catch (error) {
      if (error.response && error.response.data.status === 400) {
        Alert.alert('Error', error.response.data.toString());
      } else {
        Alert.alert('Error', error.toString());
      }
    } finally {
      setLoading(false);
    }
  };

  const validateEmail = () => {
    if (!email) {
      setEmailError('Please enter your email');
      return false;
    } else if (!emailRegex.test(email)) {
      setEmailError('Please enter a valid email address');
      return false;
    } else {
      setEmailError('');
      return true;
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View style={{alignItems: 'center', flexDirection: 'column'}}>
            <Image
              source={require('../../assets/images/tptLogo.png')}
              style={styles.logoStyle}
            />
          </View>
          <Text style={styles.loginText}> Forgot password</Text>
          <Text style={styles.resetPassowrd}>
            Please enter your email to reset the password
          </Text>
          <View style={{marginTop: 40}}>
            <Text style={styles.emailText}>Email</Text>
            <InputText
              placeholder={'Enter your email'}
              handleOnchange={text => {
                setEmail(text);
                setEmailError('');
              }}
              keyboardType="email-address"
              value={email}
            />
            {emailError ? (
              <Text style={styles.errorStyle}>{emailError}</Text>
            ) : null}
          </View>
        </View>

        {loading && <MyActivityIndicator visible={true} />}
        <View style={{marginHorizontal: 20, marginBottom: 10}}>
          <ButtonLoader
            label={'Reset Password'}
            onPress={handleSignIn}
            containerStyle={styles.buttonStyle}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ForgotPassword;
