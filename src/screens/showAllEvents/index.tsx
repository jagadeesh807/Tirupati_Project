import {View, FlatList, StyleSheet} from 'react-native';
import React from 'react';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import BackButton from '../../components/BackButton';
import TPTLogo from '../../components/CustomLogo';
import CustomSearchBar from '../../components/CustomSearchBar';
import AllEventCard from '../../components/allEventCard';
import styles from './styles';

const ShowAllEvents = () => {
  // Sample data for events
  const events = [
    {
      id: '1',
      backgroundImage: require('../../assets/images/Background.png'),
      clockImage: require('../../assets/images/Clock.png'),
      date: '24/09/2024',
      artistFirstName: 'Anirudh',
      artistLastName: 'Ravichander',
      location: 'Concert - Tirupathi',
      bookNowImage: require('../../assets/images/LeftSide.png'),
    },
    {
      id: '2',
      backgroundImage: require('../../assets/images/Background.png'),
      clockImage: require('../../assets/images/Clock.png'),
      date: '25/09/2024',
      artistFirstName: 'Arijit',
      artistLastName: 'Singh',
      location: 'Concert - Bangalore',
      bookNowImage: require('../../assets/images/LeftSide.png'),
    },
    {
      id: '4',
      backgroundImage: require('../../assets/images/Background.png'),
      clockImage: require('../../assets/images/Clock.png'),
      date: '24/09/2024',
      artistFirstName: 'Anirudh',
      artistLastName: 'Ravichander',
      location: 'Concert - Tirupathi',
      bookNowImage: require('../../assets/images/LeftSide.png'),
    },
    {
      id: '3',
      backgroundImage: require('../../assets/images/Background.png'),
      clockImage: require('../../assets/images/Clock.png'),
      date: '25/09/2024',
      artistFirstName: 'Arijit',
      artistLastName: 'Singh',
      location: 'Concert - Bangalore',
      bookNowImage: require('../../assets/images/LeftSide.png'),
    },
    // Add more events as needed
  ];

  // Render item for FlatList
  const renderItem = ({item}) => (
    <AllEventCard
      backgroundImage={item.backgroundImage}
      clockImage={item.clockImage}
      date={item.date}
      artistFirstName={item.artistFirstName}
      artistLastName={item.artistLastName}
      location={item.location}
      bookNowImage={item.bookNowImage}
    />
  );

  return (
    <WrapperContainer>
      <View style={styles.container}>
        <View style={styles.header}>
          <BackButton />
          <View style={styles.logoContainer}>
            <TPTLogo />
          </View>
          <View style={styles.placeholder} />
        </View>
        <View style={styles.searchBar}>
          <CustomSearchBar
            placeholder={'Search events'}
            onChangeText={text => console.log(text)}
            onSearchButtonPress={() => console.log('Search pressed')}
            onCancelButtonPress={() => console.log('Cancel pressed')}
          />
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={events}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          contentContainerStyle={styles.listContent}
        />
      </View>
    </WrapperContainer>
  );
};

export default ShowAllEvents;
