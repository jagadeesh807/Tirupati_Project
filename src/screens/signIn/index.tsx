import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import InputText from '../../components/InputText';
import ButtonLoader from '../../components/ButtonLoader';
import {SCREENS} from '..';
import {emailRegex} from '../../utils/validations';
import axiosInstance from '../../data/service/api';
import {API_URL} from '../../utils/apiConstant';
import MyActivityIndicator from '../../components/ActivityIndicator'; // Import MyActivityIndicator component
import AsyncStorage from '@react-native-async-storage/async-storage';
import getToken from '../../utils/token';

const SignIn = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [loading, setLoading] = useState(false); // State to manage loading indicator

  const handleSignIn = async () => {
    const isEmailValid = validateEmail();
    const isPasswordValid = validatePassword();

    if (!isEmailValid || !isPasswordValid) {
      return;
    }
    try {
      setLoading(true);
      const token = await getToken(); // Retrieve the token
      if (!token) {
        Alert.alert('Error', 'Token not found. Please log in again.');
        return;
      }
      const response = await axiosInstance.post(
        API_URL.SIGN_IN,
        {
          email: email,
          password: password,
          role: 'User',
          isLogin: 'yes',
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      if (response.status === 200) {
        const responseData = response.data.response;
        const id = responseData.id;
        await AsyncStorage.setItem('userId', id);
        navigation.navigate(SCREENS.FEED);
      } else {
        let errorMessage = 'Unknown error occurred';
        if (response.data && response.data.message) {
          errorMessage = response.data.message;
        }
        Alert.alert('Registration Failed', errorMessage);
      }
    } catch (error) {
      if (error.response) {
        Alert.alert('Error', error.response.data.error);
      } else if (error.request) {
        Alert.alert('Error', 'No response received from the server');
      }
    } finally {
      setLoading(false);
    }
  };

  const validateEmail = () => {
    if (!email) {
      setEmailError('Please enter your email');
      return false;
    } else if (!emailRegex.test(email)) {
      setEmailError('Please enter a valid email address');
      return false;
    } else {
      setEmailError('');
      return true;
    }
  };

  const validatePassword = () => {
    if (!password) {
      setPasswordError('Please enter your password');
      return false;
    } else {
      setPasswordError('');
      return true;
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}>
        <ScrollView
          contentContainerStyle={styles.container}
          keyboardShouldPersistTaps="handled">
          <View style={{alignItems: 'center', flexDirection: 'column'}}>
            <Image
              source={require('../../assets/images/tptLogo.png')}
              style={styles.logoStyle}
            />
          </View>
          <View style={{marginTop: 40}}>
            <Text style={styles.welcomeText}>Welcome</Text>
            <Text style={styles.signUpHeaderText}>
              Sign into your tirupatifyi app !
            </Text>
            <Text style={styles.emailText}>Login</Text>
            <InputText
              placeholder={'Enter Mail ID'}
              handleOnchange={text => {
                setEmail(text);
                setEmailError('');
              }}
              keyboardType="email-address"
              value={email}
            />
            {emailError ? (
              <Text style={styles.errorStyle}>{emailError}</Text>
            ) : null}
            <InputText
              placeholder={'Password'}
              handleOnchange={text => {
                setPassword(text);
                setPasswordError('');
              }}
              keyboardType="default"
              value={password}
            />
            {passwordError ? (
              <Text style={styles.errorStyle}>{passwordError}</Text>
            ) : null}
          </View>
          {loading && <MyActivityIndicator visible={true} />}
          <View style={styles.viewStyle}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 8,
              }}>
              {/* <CustomCheckbox
                isChecked={false}
                onChange={() => {}}
                disabled={false}
              />
              <Text style={styles.remeberText}>Remember me </Text> */}
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate(SCREENS.FORGOT_PASSWORD)}>
              <Text style={styles.forgetPassword}>Forget your password ?</Text>
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 25, marginHorizontal: 20}}>
            <ButtonLoader
              label={'Login'}
              onPress={handleSignIn}
              containerStyle={styles.buttonStyle}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <View style={styles.signUpView}>
        <Text style={styles.accountText}>Don't have an account?</Text>
        <TouchableOpacity onPress={() => navigation.navigate(SCREENS.SIGN_UP)}>
          <Text style={styles.signUpText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default SignIn;
