import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
  },
  logoStyle: {
    alignSelf: 'center',
  },
  loginText: {
    fontFamily: 'Rubik',
    fontSize: 32,
    fontWeight: '700',
    paddingTop: 20,
    color: '#151615',
  },
  welcomeText: {
    fontFamily: 'Rubik',
    fontSize: 18,
    fontWeight: '600',
    paddingHorizontal: 22,
    paddingTop: 2,
    color: '#000000',
  },
  emailText: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '600',
    paddingHorizontal: 22,
    padding: 4,
    color: '#000000',
  },
  signUpHeaderText: {
    fontFamily: 'Rubik',
    fontSize: 16,
    fontWeight: '300',
    paddingHorizontal: 22,
    color: '#A1A1A1',
    paddingTop: 10,
    paddingBottom: 28,
  },
  remeberText: {
    paddingLeft: 8,
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '400',
  },
  forgetPassword: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '400',
    color: '#EE8659',
    paddingTop: 8,
  },
  viewLine: {
    height: 2,
    backgroundColor: '#E1E1E1',
    marginTop: 20,
    width: '45%',
  },
  viewStyle: {
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  Or: {
    paddingTop: 10,
    color: '#E1E1E1',
    fontFamily: 'Rubik',
    fontSize: 16,
    fontWeight: '600',
  },
  signUpView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  accountText: {
    fontFamily: 'Rubik',
    fontSize: 16,
    marginRight: 10,
  },
  signUpText: {
    fontFamily: 'Rubik',
    fontSize: 16,
    fontWeight: '400',
    color: '#EE8659',
  },
  errorStyle: {
    color: 'red',
    marginHorizontal: 20,
  },
  buttonStyle: {
    backgroundColor: '#EE4266',
    // height: 50,
  },
});

export default styles;
