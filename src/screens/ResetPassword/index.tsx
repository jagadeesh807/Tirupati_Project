import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import styles from './styles';
import InputText from '../../components/InputText';
import ButtonLoader from '../../components/ButtonLoader';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '..';
import {emailRegex, passwordRegex} from '../../utils/validations';
import axios from 'axios';
import axiosInstance from '../../data/service/api';
import {API_URL} from '../../utils/apiConstant';
import MyActivityIndicator from '../../components/ActivityIndicator';

const ResetPassword = () => {
  const navigation = useNavigation();
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [newPasswordError, setNewPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [loading, setLoading] = useState(false);

  const handleUpdatePassword = async () => {
    setNewPasswordError('');
    setConfirmPasswordError('');
    setEmailError('');

    let hasError = false;

    // Validate email
    if (!email) {
      setEmailError('Please enter your email');
      hasError = true;
    } else if (!emailRegex.test(email)) {
      setEmailError('Please enter a valid email address');
      hasError = true;
    }

    // Validate new password
    // Validate new password
    if (newPassword === '') {
      setNewPasswordError('Please enter a New password');
      hasError = true;
    } else if (newPassword.length < 8) {
      setNewPasswordError('Please enter a 8 letter password');
      hasError = true;
    }

    // Validate confirm password
    if (confirmPassword === '') {
      setConfirmPasswordError('Please enter a Confirm password');
      hasError = true;
    } else if (newPassword !== confirmPassword) {
      setConfirmPasswordError('Passwords do not match');
      hasError = true;
    }
    if (hasError) {
      return;
    }
    setLoading(true);

    try {
      const response = await axiosInstance.post(API_URL.REST_PASSWORD, {
        email: email,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
      });

      if (response.status === 200) {
        navigation.navigate(SCREENS.SIGN_IN);
      } else {
        Alert.alert('Error', response.data);
      }
    } catch (error) {
      if (error.response && error.response.status === 400) {
        Alert.alert('Error', error.response.data);
      } else {
        Alert.alert('Error', error.response.data);
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}>
      <ScrollView
        contentContainerStyle={styles.container}
        keyboardShouldPersistTaps="handled">
        <View style={{alignItems: 'center', flexDirection: 'column'}}>
          <Image
            source={require('../../assets/images/tptLogo.png')}
            style={styles.logoStyle}
          />
        </View>
        <Text style={styles.loginText}>Set a new password</Text>
        <Text style={styles.resetPassowrd}>
          Dont remeber the password, set a now one !
        </Text>
        {loading && <MyActivityIndicator visible={true} />}
        <View style={{marginTop: 40}}>
          <Text style={styles.emailText}>Account Information</Text>
          <InputText
            placeholder={'Enter your email'}
            handleOnchange={text => {
              setEmail(text);
              setEmailError('');
            }}
            keyboardType="email-address"
            value={email}
          />
          {emailError ? (
            <Text style={styles.errorStyle}>{emailError}</Text>
          ) : null}
          <InputText
            placeholder={'Enter your new Password'}
            handleOnchange={text => {
              setNewPassword(text);
              setNewPasswordError('');
            }}
            keyboardType="default"
            value={newPassword}
            secureTextEntry={true}
          />
          {newPasswordError ? (
            <Text style={styles.errorStyle}>{newPasswordError}</Text>
          ) : null}
          <InputText
            placeholder={'Confirm your password'}
            handleOnchange={text => {
              setConfirmPassword(text);
              setConfirmPasswordError('');
            }}
            keyboardType="default"
            value={confirmPassword}
            secureTextEntry={true}
          />
          {confirmPasswordError ? (
            <Text style={styles.errorStyle}>{confirmPasswordError}</Text>
          ) : null}
        </View>
        <View
          style={{
            marginTop: 25,
            marginHorizontal: 20,
          }}>
          <ButtonLoader
            label={'Update password'}
            onPress={handleUpdatePassword}
            containerStyle={styles.buttonStyle}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default ResetPassword;
