import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    
  },
  logoStyle: {
    alignSelf: 'center',
  },
  loginText: {
    fontFamily: 'Rubik',
    fontSize: 18,
    fontWeight: '600',
    paddingTop: 20,
    color: '#151615',
    marginHorizontal: 20,
  },
  resetPassowrd: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '600',
    paddingTop: 20,
    color: '#989898',
    marginHorizontal: 20,
  },
  emailText: {
    fontFamily: 'Rubik',
    fontSize: 14,
    fontWeight: '500',
    paddingHorizontal: 22,
    padding: 4,
    color: '#595959',
  },

  errorStyle: {
    color: 'red',
    marginHorizontal: 20,
  },
  buttonStyle: {
    backgroundColor: '#EE4266',
    // height: 50,
  },
  screencheck: {
    backgroundColor: 'green',
  },
});

export default styles;
