import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  headerStyle: {
    fontSize: 18,
    fontFamily: 'Rubik',
    fontWeight: '400',
    color: '#000000',
  },
  viewAll: {
    fontSize: 16,
    fontFamily: 'Rubik',
    fontWeight: '400',
    color: '#000000',
  },
  imageContainer: {
    borderRadius: 6,
    borderColor: '#A6BAD0',
    borderWidth: 0.5,
    width: 100,
    height: 100,
    marginTop: 20,
    marginRight: 15,
  },
  imageStyle: {
    // marginTop: 10,
    // width: 126,
    // height: 78,
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
});

export default styles;
