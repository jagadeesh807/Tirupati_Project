import {View, Text, TouchableOpacity} from 'react-native';
import React, {useRef, useState} from 'react';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import styles from './styles';
import {ScrollView} from 'react-native';
import ServicesCardComponent from '../../components/placesCardComponent';
import SearchBar from 'react-native-search-bar';

const Services = () => {
  const [searchText, setSearchText] = useState('');
  const searchBarRef = useRef(null);
  const [selectedPlaceIndex, setSelectedPlaceIndex] = useState(null);

  const handleSearch = () => {
    searchBarRef.current?.blur();
  };

  const handleCancel = () => {
    setSearchText('');
  };

  return (
    <WrapperContainer>
      <SearchBar
        placeholder="Search"
        onChangeText={setSearchText}
        onSearchButtonPress={handleSearch}
        onCancelButtonPress={handleCancel}
        ref={searchBarRef}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <ServicesCardComponent />
        </View>
        <ServicesCardComponent />
      </ScrollView>
    </WrapperContainer>
  );
};

export default Services;
