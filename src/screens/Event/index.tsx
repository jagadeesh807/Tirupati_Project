import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import TPTLogo from '../../components/CustomLogo';
import WrapperContainer from '../../components/WrapperContainer/WrapperContainer';
import CustomSearchBar from '../../components/CustomSearchBar';
import EventCards from '../../components/eventsCard';
import {ScrollView} from 'react-native-gesture-handler';
import ButtonLoader from '../../components/ButtonLoader';
import {useNavigation} from '@react-navigation/native';
import {SCREENS, ShowAllEvents} from '..';
import ShowAllEventCards from '../../components/ShowAllEvents';

const Events = () => {
  const navigation = useNavigation();
  const eventsData = [
    {
      id: '1',
      image: require('../../assets/images/Tirupati1.jpg'),
      title: 'Event 1 Title',
    },
    {
      id: '2',
      image: require('../../assets/images/tirupati4.png'),
      title: 'Event 54 Title',
    },
    {
      id: '3',
      image: require('../../assets/images/Tirupati3.png'),
      title: 'Event 1 Title',
    },
    {
      id: '4',
      image: require('../../assets/images/Tirupati1.jpg'),
      title: 'Event 54 Title',
    },
    // Add more events as needed
  ];

  const SportsData = [
    {
      id: '1',
      image: require('../../assets/images/image41.png'),
      title: 'Event 1 Title',
    },
    {
      id: '2',
      image: require('../../assets/images/image41.png'),
      title: ' Title',
    },
    {
      id: '3',
      image: require('../../assets/images/image43.png'),
      title: 'Event 1 Title',
    },
    {
      id: '4',
      image: require('../../assets/images/image41.png'),
      title: 'Event 1 Title',
    },
    {
      id: '5',
      image: require('../../assets/images/image41.png'),
      title: ' Title',
    },
    {
      id: '6',
      image: require('../../assets/images/image43.png'),
      title: 'Event 1 Title',
    },
    // Add more events as needed
  ];
  const AllEventData = [
    {
      id: 1,
      image: require('../../assets/images/image44.png'),
    },
    {
      id: 2,
      image: require('../../assets/images/Image1.png'),
    },
    {
      id: 3,
      image: require('../../assets/images/Image2.png'),
    },
    {
      id: 4,
      image: require('../../assets/images/image44.png'),
    },
  ];
  const renderEventCard = ({item}) => (
    <EventCards image={item.image} title={item.title} />
  );

  const renderSportsCard = ({item}) => (
    <EventCards image={item.image} title={item.title} />
  );

  const renderAllEventData = ({item}) => (
    <ShowAllEventCards image={item.image} />
  );
  return (
    <WrapperContainer>
      <View style={styles.container}>
        <TPTLogo />
        <View style={styles.searchBarContainer}>
          <CustomSearchBar
            placeholder={'Search events'}
            onChangeText={text => console.log(text)}
            onSearchButtonPress={() => console.log('Search pressed')}
            onCancelButtonPress={() => console.log('Cancel pressed')}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
            <FlatList
              data={eventsData}
              renderItem={renderEventCard}
              keyExtractor={item => item.id}
              contentContainerStyle={styles.flatListContent}
              horizontal
            />
            <View style={styles.thingsToDoContainer}>
              <Text style={styles.thingsToDoTitle}>
                Things to do in Tirupati
              </Text>
              <Text style={styles.thingsToDoSubtitle}>
                Here’s what everyone is booking
              </Text>
              <View style={styles.eventDetailContainer}>
                <Image
                  source={require('../../assets/images/SampleImage.png')}
                  style={styles.eventImage}
                />
                <View style={styles.eventTextContainer}>
                  <Text style={styles.textcolor}>Avesham - Movie (EU)</Text>
                  <Text style={styles.eventGenre}>Comedy, Action</Text>
                  <Text style={styles.eventDescription}>
                    Aavesham (transl. Excitement)
                  </Text>
                  <Text style={styles.textcolor}>150 ₹ Onwards</Text>
                </View>
              </View>
              <View style={styles.bookNowButtonContainer}>
                <ButtonLoader
                  label={'Book Now'}
                  onPress={function (): void {
                    throw new Error('Function not implemented.');
                  }}
                />
              </View>
              <Text></Text>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontSize: 16, marginTop: 15, color: '#242424'}}>
                Choices Vast But Filling Fast
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate(SCREENS.SHOW_ALL_EVENTS)}>
                <Text style={{fontSize: 16, color: '#EE4266', marginTop: 10}}>
                  View All
                </Text>
              </TouchableOpacity>
            </View>
            <Text>Hurry, explore our range of fun events</Text>

            <FlatList
              data={SportsData}
              renderItem={renderSportsCard}
              keyExtractor={item => item.id}
              contentContainerStyle={styles.flatListContent}
              horizontal
            />
            <View>
              <FlatList
                data={AllEventData}
                renderItem={renderAllEventData}
                keyExtractor={item => item.id}
                contentContainerStyle={styles.flatListContent}
                horizontal
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontSize: 16, marginTop: 10, color: '#242424'}}>
                Adventure Bucket List
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate(SCREENS.SHOW_ALL_EVENTS)}>
                <Text style={{fontSize: 16, color: '#EE4266', marginTop: 8}}>
                  View All
                </Text>
              </TouchableOpacity>
            </View>
            <Text>Here’s what we’ve put together for you</Text>
            <FlatList
              data={SportsData}
              renderItem={renderSportsCard}
              keyExtractor={item => item.id}
              contentContainerStyle={styles.flatListContent}
              horizontal
            />
            <View>
              <FlatList
                data={AllEventData}
                renderItem={renderAllEventData}
                keyExtractor={item => item.id}
                contentContainerStyle={styles.flatListContent}
                horizontal
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </WrapperContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBarContainer: {
    marginTop: 10,
    marginHorizontal: 10,
  },
  flatListContent: {
    marginTop: 10,
  },
  textcolor: {
    color: '#ffffff',
  },
  thingsToDoContainer: {
    backgroundColor: '#242424F2',
    marginTop: 15,
    borderRadius: 15,
  },
  thingsToDoTitle: {
    color: '#ffffff',
    marginTop: 10,
    marginHorizontal: 10,
  },
  thingsToDoSubtitle: {
    color: '#B0B0B0',
    marginHorizontal: 10,
  },
  eventDetailContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  eventImage: {
    height: 160,
    width: 140,
    resizeMode: 'cover',
    marginTop: 10,
    marginHorizontal: 10,
  },
  eventTextContainer: {
    marginHorizontal: 5,
  },
  eventGenre: {
    color: '#B0B0B0',
  },
  eventDescription: {
    color: '#B0B0B0',
    marginTop: 15,
    textAlign: 'center',
  },
  bookNowButtonContainer: {
    marginHorizontal: 10,
    marginTop: 20, // Adjust this value to create the desired gap
  },
});

export default Events;
