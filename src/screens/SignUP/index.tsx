import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  Alert,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
} from 'react-native';
import InputText from '../../components/InputText';
import ButtonLoader from '../../components/ButtonLoader';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import {SCREENS} from '..';
import AxiosResponse from 'axios';
import {emailRegex} from '../../utils/validations';
import MyActivityIndicator from '../../components/ActivityIndicator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axiosInstance from '../../data/service/api';
import {API_URL} from '../../utils/apiConstant';

const SignUp = () => {
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [userNameError, setUserNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  const handleSignup = async () => {
    let isValid = true;

    if (userName.trim() === '') {
      setUserNameError('Enter UserName');
      isValid = false;
    } else if (userName.length < 3) {
      setUserNameError('UserName Should be at least 3 characters');
      isValid = false;
    } else {
      setUserNameError('');
    }

    if (email.trim() === '') {
      setEmailError('Enter Email');
      isValid = false;
    } else if (!emailRegex.test(email)) {
      setEmailError('Invalid email format');
      isValid = false;
    } else {
      setEmailError('');
    }

    if (password.trim() === '') {
      setPasswordError('Enter Password');
      isValid = false;
    } else if (password.length < 8) {
      setPasswordError('Password must be at least 8 characters');
      isValid = false;
    } else {
      setPasswordError('');
    }

    if (!isValid) {
      return;
    }

    setLoading(true); // Start loading indicator

    try {
      const response = await axiosInstance.post(API_URL.SIGN_UP, {
        email,
        password,
        role: 'User',
        userName,
        isLogin: 'no',
      });

      if (response.status === 200) {
        const responseData = response.data.response;
        const token = responseData.token;
        await AsyncStorage.setItem('token', token);
        navigation.navigate(SCREENS.SIGN_IN);
      } else {
        let errorMessage = 'Unknown error occurred';
        if (response.data && response.data.message) {
          errorMessage = response.data.message;
        }
        Alert.alert('Registration Failed', errorMessage);
      }
    } catch (error) {
      if (error.response) {
        Alert.alert('Error', error.response.data.error);
      } else if (error.request) {
        Alert.alert('Error', 'No response received from the server');
      }
    }

    setLoading(false);
  };

  return (
    <SafeAreaView>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 0}>
        <ScrollView
          contentContainerStyle={styles.container}
          keyboardShouldPersistTaps="handled">
          <View style={{flex: 1}}>
            <View style={styles.container}>
              <View style={{alignItems: 'center', flexDirection: 'column'}}>
                <Image
                  source={require('../../assets/images/tptLogo.png')}
                  style={styles.logoStyle}
                />
              </View>
              <View style={{marginTop: 40}}>
                <Text style={styles.welcomeText}>
                  New user to tirupatifyi app
                </Text>
                <Text style={styles.signUpHeaderText}>Join with us now !</Text>
                <Text style={styles.emailText}>SignUp</Text>
                <InputText
                  placeholder={'Enter your Name'}
                  handleOnchange={text => {
                    setUserName(text);
                    setUserNameError('');
                  }}
                  keyboardType="default"
                  value={userName}
                />
                {userNameError ? (
                  <Text style={styles.errorText}>{userNameError}</Text>
                ) : null}
                <InputText
                  placeholder={'Enter Mail Id'}
                  handleOnchange={text => {
                    setEmail(text);
                    setEmailError('');
                  }}
                  keyboardType="email-address"
                  value={email}
                />
                {emailError ? (
                  <Text style={styles.errorText}>{emailError}</Text>
                ) : null}
                <InputText
                  placeholder={'Password'}
                  handleOnchange={text => {
                    setPassword(text);
                    setPasswordError('');
                  }}
                  keyboardType="default"
                  value={password}
                />
                {passwordError ? (
                  <Text style={styles.errorText}>{passwordError}</Text>
                ) : null}
              </View>
              {loading && <MyActivityIndicator visible={true} />}
              <View style={{marginTop: 25, marginHorizontal: 20}}>
                <ButtonLoader
                  label={'Signup'}
                  onPress={handleSignup}
                  containerStyle={styles.buttonStyle}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default SignUp;
