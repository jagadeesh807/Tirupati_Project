import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import styles from './style';
import {SCREENS} from '..';
import {SafeAreaView} from 'react-native-safe-area-context';

const WelcomeScreen = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView>
      <View style={styles.container}>
        {/* <ImageBackground
        source={require('../../assets/images/welcomeScreen.png')}
        style={styles.imageContainer}> */}
        <View>
          <Image
            source={require('../../assets/images/tptLogo.png')}
            style={styles.logo}
          />
          <Text style={styles.tirupatiWelcomText}>Welcome To Tirupatifyi</Text>
          <Text style={styles.subTitleContainer}>
            Tirupathi Connect: All Services, One App, Your City's Lifeline!
          </Text>
        </View>
        <View
          style={{
            // flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 25,
          }}>
          <Image
            style={{
              resizeMode: 'center',
              width: 300,
              height: 300,
              borderRadius: 5,
            }}
            source={require('../../assets/images/TiruptiWelcome.png')}
          />
        </View>
        <View style={styles.welcomeTextStyle}>
          <TouchableOpacity
            onPress={() => navigation.navigate(SCREENS.SIGN_IN)}
            style={styles.startButton}>
            <Text style={styles.getStarted}>Get Started</Text>
          </TouchableOpacity>
        </View>
        {/* </ImageBackground> */}
      </View>
    </SafeAreaView>
  );
};

export default WelcomeScreen;
