import {Platform, StyleSheet} from 'react-native';
import {green} from 'react-native-reanimated/lib/typescript/reanimated2/Colors';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  imageContainer: {
    flex: 1,
    resizeMode: 'center',
  },
  logo: {
    margin: 35,
    width: 90,
    height: 70,
    alignSelf: 'center',
  },
  tirupatiWelcomText: {
    textAlign: 'center',
    fontFamily: 'Rubik',
    fontSize: 30,
    fontWeight: '900',
    color: '#f47658',
  },

  welcomeText: {
    color: '#ffffff',
    fontFamily: 'IbarraRealNova',
    fontWeight: '400',
    fontSize: 16,
    top: 60,
  },
  startButton: {
    alignItems: 'center',
    backgroundColor: '#EE4266',
    width: '90%',
    height: '25%',
    justifyContent: 'center',
    borderRadius: 10,
    top: Platform.OS === 'ios' ? 40 : 90,
  },
  getStarted: {
    fontFamily: 'Rubik',
    fontSize: 20,
    fontWeight: '400',
    color: '#ffffff',
  },
  welcomeTextStyle: {
    alignItems: 'center',
  },
  subTitleContainer: {
    textAlign: 'center',
    marginHorizontal: 40,
    fontWeight: '400',
    fontFamily: 'Rubik',
    fontSize: 18,
    marginTop: 8,
    color: '#A1A1A1',
  },
});

export default styles;
