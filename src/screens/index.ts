import {IListOfScreens} from '../types';
import Loader from './loader';
import SignIn from './signIn';
import WelcomeScreen from './welcom';
import SignUp from './SignUP';
import ForgotPassword from './forgotPassword';
import ResetPassword from './ResetPassword';
import Feed from './feed';
import InternalJob from './JobPosting';
import Events from './Event';
import PlaceAround from './PlacesAround';
import Services from './Services';
import CreateFeed from './feed/CreateFeed';
import JobDetails from './JobDetails';
import PlaceAroundList from './PlaceAroundList';
import ShowAllEvents from './showAllEvents';
import AllServices from './allServices';
import ServicesDetails from './ServicesDetails';

export {default as SignIn} from './signIn';
export {default as Loader} from './loader';
export {default as WelcomeScreen} from './welcom';
export {default as SignUp} from './SignUP';
export {default as Feed} from './feed';
export {default as PlaceAround} from './PlacesAround';
export {default as Services} from './Services';
export {default as CreateFeed} from './feed/CreateFeed';
export {default as JobDetails} from './JobDetails';
export {default as placeAroundList} from './PlaceAroundList';
export {default as ShowAllEvents} from './showAllEvents';
export {default as AllServices} from './allServices';
export {default as ServicesDetails} from './ServicesDetails';

export const SCREENS: {[key: string]: never} = {
  FEED: 'Feed',
  SIGN_IN: 'SignIn',
  LOADER: 'Loader',
  WELCOME_SCREENS: 'WelcomeScreen',
  SIGN_UP: 'SignUp',
  FORGOT_PASSWORD: 'ForgotPassword',
  RESET_PASSWORD: 'ResetPassword',
  INTERNAL_JOB: 'InternalJob',
  EVENT: 'Events',
  PLACE_AROUND: 'PlaceAround',
  SERVICES: 'Services',
  CREATE_FEED: 'CreateFeed',
  JOB_DETAILS: 'JobDetails',
  PLACE_AROUND_LIST: 'PlaceAroundList',
  SHOW_ALL_EVENTS: 'ShowAllEvents',
  ALL_SERVICES: 'AllServices',
  SERVICES_DETAILS: 'ServicesDetails',
};

export const STACKED_SCREENS: IListOfScreens[] = [
  {
    name: SCREENS.FEED,
    component: Feed,
  },
];

export const PRE_LOGIN_STACK: IListOfScreens[] = [
  // {
  //   name: SCREENS.ONBOARD,
  //   component: OnBoard,
  // },
  {
    name: SCREENS.SIGN_IN,
    component: SignIn,
  },
  {
    name: SCREENS.SIGN_UP,
    component: SignUp,
  },
  {
    name: SCREENS.FORGOT_PASSWORD,
    component: ForgotPassword,
  },
  {
    name: SCREENS.RESET_PASSWORD,
    component: ResetPassword,
  },
  {
    name: SCREENS.EVENT,
    component: Events,
  },
  {
    name: SCREENS.LOADER,
    component: Loader,
  },
];
export const ONCE_LOGIN_STACK: IListOfScreens[] = [
  {
    name: SCREENS.SIGN_IN,
    component: SignIn,
  },
  {
    name: SCREENS.LOADER,
    component: Loader,
  },
];
export const BOTTOM_TAB_SCREENS: IListOfScreens[] = [
  {
    name: SCREENS.FEED,
    component: Feed,
  },
  {
    name: SCREENS.INTERNAL_JOB,
    component: InternalJob,
  },
  {
    name: SCREENS.JOB_DETAILS,
    component: JobDetails,
  },
  {
    name: SCREENS.PLACE_AROUND_LIST,
    component: PlaceAroundList,
  },
  {
    name: SCREENS.SHOW_ALL_EVENTS,
    component: ShowAllEvents,
  },
];
