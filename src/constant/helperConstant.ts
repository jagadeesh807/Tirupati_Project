import {Dimensions} from 'react-native';

export const SCREEN_HEIGHT = Dimensions.get('screen').height;
export const SCREEN_WIDTH = Dimensions.get('screen').width;

export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const WINDOW_WIDTH = Dimensions.get('window').width;

export const AUDIENCE = ['All Groups', 'Hr Group', 'Leadership Group'];

export const DROPDOWN_LIST = [
  'Spouse',
  'Parent',
  'Sibling',
  'Friend',
  'Cousin',
  'Guardian',
];

export const LEAVE_LIST = [
  'Casual Leave',
  'Sick Leave',
  'Bereavement Leave',
  'Maternity Leave',
];
