import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  AllServices,
  CreateFeed,
  JobDetails,
  SCREENS,
  ShowAllEvents,
  SignIn,
  SignUp,
  WelcomeScreen,
} from '../screens';
import ForgotPassword from '../screens/forgotPassword';
import ResetPassword from '../screens/ResetPassword';
import BottomTab from './bottomTab';
import PlaceAroundList from '../screens/PlaceAroundList';
import {Linking} from 'react-native';

const Stack = createStackNavigator();

function StackSelector() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={SCREENS.WELCOME_SCREENS} component={WelcomeScreen} />
      <Stack.Screen name={SCREENS.SIGN_IN} component={SignIn} />
      <Stack.Screen name={SCREENS.SIGN_UP} component={SignUp} />
      <Stack.Screen name={SCREENS.FORGOT_PASSWORD} component={ForgotPassword} />
      <Stack.Screen name={SCREENS.RESET_PASSWORD} component={ResetPassword} />
      <Stack.Screen name={SCREENS.CREATE_FEED} component={CreateFeed} />
      <Stack.Screen name={SCREENS.FEED} component={BottomTab} />
      <Stack.Screen name={SCREENS.JOB_DETAILS} component={JobDetails} />
      <Stack.Screen
        name={SCREENS.PLACE_AROUND_LIST}
        component={PlaceAroundList}
      />
      <Stack.Screen name={SCREENS.SHOW_ALL_EVENTS} component={ShowAllEvents} />
      <Stack.Screen name={SCREENS.ALL_SERVICES} component={AllServices} />
      <Stack.Screen name={SCREENS.SERVICES_DETAILS} component={AllServices} />
    </Stack.Navigator>
  );
}

export default StackSelector;
