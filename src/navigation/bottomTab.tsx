import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Feed from '../../src/screens/feed';
import InternalJob from '../screens/JobPosting';
import {Image} from 'react-native';
import {PlaceAround, Services} from '../screens';
import Events from '../screens/Event';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#EE4266',
      }}>
      <Tab.Screen
        name="Feed"
        component={Feed}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Image
              source={require('../assets/images/Feeds.png')}
              style={{width: 20, height: 20, tintColor: color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Job"
        component={InternalJob}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Image
              source={require('../assets/images/Job.png')}
              style={{width: 30, height: 30, tintColor: color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Places Around"
        component={PlaceAround}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Image
              source={require('../assets/images/PacesAround.png')}
              style={{width: 25, height: 25, tintColor: color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Events"
        component={Events}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Image
              source={require('../assets/images/Event.png')}
              style={{width: 30, height: 25, tintColor: color}}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Services"
        component={Services}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Image
              source={require('../assets/images/Services.png')}
              style={{width: 30, height: 25, tintColor: color}}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;
