export const BaseURL = 'http://35.155.73.184/api/';

export enum API_URL {
  SIGN_UP = 'register',
  SIGN_IN = 'login',
  EMAIL_VERIFICATION = 'Auth/forgotpassword',
  REST_PASSWORD = 'Auth/resetpassword',
  FEED_GET = 'getposts',
  FEED_POST = 'addpost',
}
