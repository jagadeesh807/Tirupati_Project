import {Platform} from 'react-native';

export const askContactPermissions = async () => {
  if (Platform.OS === 'android') {
    return true;
  } else {
    return false;
  }
};
