export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const passwordRegex = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
