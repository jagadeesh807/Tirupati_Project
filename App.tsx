import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StackSelector from './src/navigation/stackSelector';

const App = () => {
  return (
    <NavigationContainer>
      <StackSelector />
    </NavigationContainer>
  );
};

export default App;
